from __future__ import print_function

import flask
import grovepi
import sys


light_sensor_port = 0
temphum_sensor_port = 4

app = flask.Flask(__name__)

@app.route("/light_sensor")
def light_sensor():
	global light_sensor_port
	
	value = "";
	try:
		value = grovepi.analogRead(light_sensor_port)
	except ex:
		value = "error";
		
	return str(grovepi.analogRead(light_sensor_port))
	
@app.route("/temphum_sensor")
def temp_humidy_sensor():
	global temphum_sensor_port
	try:
		value = grovepi.dht(temphum_sensor_port, 0)
		if len(value) == 2:
			value = str(value[0])+ ";" +str(value[1])
		else:
			value = "error"
			
	except ex:
		value = "error";
		
	return str(value)
	
if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8080, debug=True)
