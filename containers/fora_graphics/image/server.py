from __future__ import print_function

import flask
import sys
import random
import cStringIO
import base64
from io import BytesIO
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure





app = flask.Flask(__name__)

@app.route("/")
def index():	
	return "hello world: fora_graphics"
		
@app.route("/graph", methods=['GET', 'POST'])
def plot():
	json = flask.request.get_json()
	#print(str(json), file=sys.stdout)

	fig = Figure()
	axis = fig.add_subplot(1, 1, 1)	
	
	x = []
	for key, value in json.iteritems():
		xs = []
		ys = []
		for item in value:
			xs += [float(item[1])]
			ys += [float(item[0])]
		
		
		if len(xs) > 1:		
			axis.set_xlim(xs[0], xs[-1])		
		
		axis.plot(xs, ys)
		
		#print(str(xs) + " "+str(ys), file=sys.stdout)
	#xs = range(100)
	#ys = [random.randint(1, 50) for x in xs]
		
		
		
		
	
	canvas = FigureCanvas(fig)
	
	
	figfile = BytesIO()
	fig.savefig(figfile, format='png')
	figdata_png = base64.b64encode(figfile.getvalue())
	
	
	#output = cStringIO.StringIO()
	#canvas.print_png(output)
	#response = flask.make_response(output.getvalue())
	#response.mimetype = 'image/png'
	
	return figdata_png.decode('utf8');

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response
  
		
if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8082, debug=True)
