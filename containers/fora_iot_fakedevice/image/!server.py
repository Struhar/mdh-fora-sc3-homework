from __future__ import print_function

import flask
import sys

app = flask.Flask(__name__)

@app.route("/light_sensor")
def light_sensor():
	return "10"
	
@app.route("/temphum_sensor")
def temp_humidy_sensor():
	return "22.0;20.0"
		
if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8080, debug=True)
