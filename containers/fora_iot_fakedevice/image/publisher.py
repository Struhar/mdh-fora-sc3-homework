import sys
import urllib2
import time
import random

print "argv:", sys.argv

if len(sys.argv) != 4:
	print "python publisher.py device_name middleware_url sleep"
	sys.exit()
	
device_name = sys.argv[1]
address = sys.argv[2]
sleep = int(sys.argv[3])

def compose():
	return "temperature=%d&humidity=%d&light=%d" % (random.randint(1, 30),random.randint(1, 100),random.randint(1, 900))


while True:
	try:
		url = 'http://%s/update?device=%s&%s' % (address, device_name, compose())
		print url, 
		print urllib2.urlopen(url).read()
	except:
		print "unable to update the status"
	
	time.sleep(sleep)