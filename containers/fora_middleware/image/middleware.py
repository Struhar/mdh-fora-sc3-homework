from __future__ import print_function

import flask
import sys
import time
import urllib2
import os
import urllib
import json 

app = flask.Flask(__name__, template_folder='templates', static_url_path='')

database = {};
database["data"] = {}




@app.route('/static/<path:path>')
def send_static(path):
    return flask.send_from_directory('templates/static', path)
	
@app.route("/env")
def env():
	return str(os.environ)
	

@app.route("/")
def stats():
	return flask.render_template('index2.html')
	
@app.route("/ping")
def ping():
	url = 'http://analysis:8081'
	data = urllib2.urlopen(url).read()
	
	return "ping: " + str(data)
	
def add_params_to_database(params):
	global database
	
	device = params.get('device');
	
	if not device in database["data"]:
		database["data"][device] = []
	
	values = []
	for key, value in params.iteritems():
		if key != "device":
			values += [(key, value)]
	
	values += [("timestamp", time.time())]
	database["data"][device] += [values]
	
	
@app.route("/update")
def sensor_update():
	add_params_to_database(flask.request.args)
	return "middleware"

	
@app.route("/get_database")	
def get_database():
	global database
	return flask.jsonify({"database": database})

	
def filter(devices, sensor, limit):
	global database
	result = {}
	if "*" in devices:
		devices = []
		for key, value in database["data"].iteritems():
			devices += [key]
			
	for dev_item in devices:
		res = []
		if dev_item in database["data"]:
			for item in database["data"][dev_item]:
				timestamp = ""
		
				for sens in item: 
					if sens[0] == "timestamp":
						timestamp = sens[1]
			
				for sens in item: 
					if (sens[0] == sensor):
						res += [(sens[1], timestamp)]
				
			result[dev_item] = res[-limit:]

	return result

	

@app.route("/filter/<devices>/<sensor>/<limit>")
def filterMe(devices, sensor, limit):
	return json.dumps(filter(devices.split(","), sensor, int(limit))); 
	


@app.route("/graph/<devices>/<sensor>")	
def graph(devices, sensor):
	devices = devices.split(",")
	data = json.dumps(filter(devices, sensor, 20))
	
	req = urllib2.Request('http://graphics:8082/graph')
	req.add_header('Content-Type', 'application/json')
	response = urllib2.urlopen(req, data)
	
	return response.read()

@app.route("/analysis/<devices>/<sensor>")	
def analysis(devices, sensor):
	devices = devices.split(",")
	data = json.dumps(filter(devices, sensor, 100))
	
	req = urllib2.Request('http://analysis:8081/analysis')
	req.add_header('Content-Type', 'application/json')
	response = urllib2.urlopen(req, data)
	
	return response.read()

	
	
@app.route("/get_devices")	
def get_devices():
	global database
	
	devices = {}
	
	for key, value in database["data"].iteritems():
		sensors = []
		for item in database["data"][key]:
			for sens in item: 
				#print(str(item), file=sys.stdout)
				if (sens[0] != "timestamp") and (not sens[0] in sensors):
					sensors += [sens[0]]
		
		devices[key] = sensors
	
	return flask.jsonify({"devices": devices})


@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response
  
	
if __name__ == "__main__":
	print("start")
	
	'''
	params = {}
	params["device"] = "dev1"
	params["sensor1"] = "10"
	params["sensor2"] = "10"

	add_params_to_database(params)
	
	for i in range(10):
		params = {}
		params["device"] = "raspberry"
		params["sensor1"] = "10"
		params["sensor2"] = "1"

		add_params_to_database(params)
	
	'''
	app.run(host='0.0.0.0', port=80, debug=True)
