from __future__ import print_function

import flask
import sys
import json

app = flask.Flask(__name__)

@app.route("/")
def index():	
	return "hello world"

	

def avg(data):
	cnt = 0
	sum = 0
	try: 
		for item in data:
			try:
				sum += float(item[0])
				cnt += 1
			except:
				print("Cant convert", file=sys.stdout)
	except:
		return "NaN"
		
	return sum/cnt

		
def min(data):
	min = "NaN"
	try: 
		for item in data:
			try:
				val = float(item[0])
				if min == "NaN" or val < min:
					min = val
			except:
				print("Cant convert", file=sys.stdout)
	except:
		return "NaN"
		
	return min

	
def max(data):
	max = "NaN"
	try: 
		for item in data:
			try:
				val = float(item[0])
				if max == "NaN" or val > max:
					max = val
			except:
				print("Cant convert", file=sys.stdout)
	except:
		return "NaN"
		
	return max

	
@app.route("/analysis", methods=['GET', 'POST'])
def analysis():
	jsn = flask.request.get_json()
	result = {};
	
	for key, value in jsn.iteritems():
		result[key+"_average"] = avg(value)
		result[key+"_min"] = min(value)
		result[key+"_max"] = max(value)
		
		xs = []
		ys = []
		for item in value:
			xs += [item[1]]
			ys += [item[0]]
	
	
	return json.dumps(result) 




	
if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8081, debug=True)
