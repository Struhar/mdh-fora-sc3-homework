# Installation guide

## Solution overview
We provide two docker images for raspberry PI:
- **struharv/fora_iot_raspberry** - reads sensor values from raspberry PI and sends them to the middleware 
- **struharv/fora_iot_fakedevice** - emulates raspberry device with connected sensors and sends the data to the middleware. It can be used for testing purposes

Additionally, we provide three docker images for deployment in the cloud/fog, the containers are mantained in docker-compose
- **struharv/fora_middleware** - centrail point of the solution, provides dynamically generated webpages, connects graphics and analysis containers, stores database
- **struharv/fora_analysis** - provides simple analysis of the data
- **struharv/fora_graphics** - generates graphs

Every image can be build by command:
- docker build -t <docker image name> .
- please do so, before executing them 


## Installation guide

### Data sources
**Fake Raspberry PI** For testing purposes, no hardware needed. It can be executed in local computer as well as in cloud:

`docker run -e device_name=<name of the device> -e interval=<interval between sending data to the cloud> -e middleware_url=<url of the middleware> struharv/fora_iot_fakedevice`

e.g. `docker run -e device_name=mdh_homework_fake -e interval=5 -e middleware_url=35.204.25.153 struharv/fora_iot_fakedevice`

**Normal Raspberry PI**, can be executed:

`docker run --privileged -e device_name=mdh_homework_normal -e interval=5 -e middleware_url=35.204.25.153 struharv/fora_iot_raspberry`

### Google Cloud Platform
1. Run VM
2. cd compose/
3. docker-compose up
4. open new browser window and type address: `http://<public ip of VM>`

Note, it is needed to add docker-compose alias in Google Cloud Platform as follows: 
1. `echo alias docker-compose="'"'docker run \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$PWD:/rootfs/$PWD" \
    -w="/rootfs/$PWD" \
    docker/compose:1.13.0'"'" >> ~/.bashrc`
2. `source ~/.bashrc`


## Failover functionality

Docker containers are supposed to be restarted automatically. The failure can be simulated as follows:
1. start the solution: docker up
2. list the running containers: `docker container ls`
3. note id of the container (e.g. you can use id of struharv/fora_graphic)
4. open bash in the container you want to fail: `docker exec -it <id of the container> bash`
5. list processes in the container: `ps aux`
6. kill the processes: `kill -9 <related processes returned in the previous step>`
7. wait few seconds
8. the container should be restarted 
9. check the webpage that the graph shown


![alt text](docs/screenshot.png)
 




